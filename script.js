const closeBurger = document.querySelector(".close-js");
const burgerButton = document.querySelector(".burger-js");
const burgerMenu = document.querySelector(".burger-menu-js");
burgerButton.addEventListener("click", () => {
    burgerMenu.classList.add("show");
});

closeBurger.addEventListener("click", () => {
    burgerMenu.classList.remove("show");
})